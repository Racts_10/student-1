/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Rachit
 */
package sheridan.rachit.arrays;

import java.util.Scanner;
import sheridan.student.Student;

import java.util.Scanner;

public class StudentList {

    public static void main(String[] args) {

        Student[] students = new Student[2];

        Scanner input = new Scanner(System.in);

        for (int i = 0; i < students.length; i++) {

            System.out.println("Enter the student's name");

            String name = input.nextLine();

            System.out.println("Enter the student's Id");

            String id = input.nextLine();

            Student student = new Student(name, id);

            students[i] = student;

        }

        System.out.println("Printing the students name and ID:");

        String format = "The student's name is %s\nand the Student Id is %s\n";

        for (Student student : students) {

            System.out.printf(format, student.getName(), student.getId());

        }

    }
}
